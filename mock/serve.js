const Koa = require('koa')
const app = new Koa()
const cors = require('koa2-cors') //跨域
const bodyParser = require('koa-bodyparser') //post参数处理
const route = require('./route/index.js') //路由

app.use(bodyParser())
app.use(cors())
app.use(route.routes(), route.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
    console.error('server error', err, ctx)
});

app.listen(3000, function() {
    console.log('成功:http://localhost:3000')
})