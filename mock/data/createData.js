var Mock = require('mockjs') //引入mockjs
const Random = Mock.Random;  //引入mockjs生成随机属性的函数 random具体可以生成
module.exports = class CreateData{
    create(length,config){
        let data = [];
        let _this = this;
        for (let i = 0; i < length; i++) {
            let obj = {};
            setObj(obj,config,i)
            data.push(obj)
        }
        function setObj(obj,config,i){
            for (let key in config) {
                const row = config[key];
                if(row=='id'){
                    obj[key] = i+1;
                }else if(typeof row === 'string'){
                    let arr = row.split('|');
                    let type = arr[0],params = arr[1];
                    if(type=='status'){
                        params = params.split(',')
                    }
                    obj[key] = _this[type](params);                
                }else if(typeof row === 'object'&&!Array.isArray(row)){
                    obj[key] = {};
                    setObj(obj[key],row)
                }else{
                    obj[key] = row;
                }
            }
        }
        return data;
    }
    image(size='100x100'){
        return Random.image(size);//size:100x100
    }
    float(){
        return Random.float(0,999,0,2)
    }    
    string(len=5){
        return Random.string('lower',len)
    }
    number(len=5){
        return Random.string('number',len)
    }
    // csentence
    status(options){//options:正常,失败
        var i = Math.floor(Math.random()*options.length);
        return options[i] || '';
    }
    date(format='yyyy-MM-dd'){//format：yyyy-MM-dd HH:mm:ss  时间戳：T
        return Random.date(format)
    }
}