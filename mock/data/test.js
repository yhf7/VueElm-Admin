const CreateData = require('./createData.js');
const obj = new CreateData();
const res = {
    code:0,
    list:obj.create(10,{
        id:'id',
        image:'image|100x100',
        title:'string|9',
        sku:'number|9',
        price:'float',
        status:'status|正常,失败',
        date:'date|yyyy-MM-dd',
        variations:obj.create(2,{
            id:'id',
            image_url:'image',
            sku:'number',
            price:'float',
            weight:'float',
            pdm_create_time:'date',
        }),
        test:{
            a:'string|9'
        }
    })
}

module.exports = res;

