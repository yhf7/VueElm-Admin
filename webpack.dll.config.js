const path = require('path')
const webpack = require('webpack')
/**
 * 尽量减小搜索范围
 * target: '_dll_[name]' 指定导出变量名字
 */
module.exports = {
    entry: {
        vendor: ['jquery', 'lodash']
    },
    output: {
        path: path.join(__dirname, 'dist'),
    },
    // manifest是描述文件
    plugins: [
        new webpack.DllPlugin({
            path: path.join(__dirname, 'dist', 'manifest.json')
        })
    ]
}