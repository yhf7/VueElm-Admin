const path = require('path')
const OfflinePlugin = require('offline-plugin');//service worker缓存
const CompressionWebpackPlugin = require('compression-webpack-plugin')//gzip
const HappyPack = require('happypack')
const os = require('os')
const happyThreadPool = HappyPack.ThreadPool({ size: os.cpus().length })
const NodemonPlugin = require( 'nodemon-webpack-plugin' ) // 更新重启-开启mock服务

const url = '127.0.0.1'
// api请求地址
// const urls = 'yhf7.top:8002'
const urls = 'elm.cangdu.org'
module.exports = {
    pages: {
        main: {
            entry: 'src/module/index/main.js',
            // 模板来源
            template: 'public/index.html',     
            filename: 'index.html',     
            // 当使用 title 选项时，
            // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
            title: ''
        },
    },
    indexPath:'index.html',
    devServer:{
        proxy: {
            '/api': {
                target: 'https://' + urls,
                changeOrigin: true,
                pathRewrite: {
                    '^/api': '/'
                }
            },
            '/mock': {
                target: 'http://localhost:3000',
                changeOrigin: true,
                pathRewrite: {
                    '^/mock': '/'
                }
            }
        },
        host:url
    },
    productionSourceMap:false,
    configureWebpack:config => {
        if(process.env.NODE_ENV === 'production'){          
            config.plugins.push(new OfflinePlugin())
            config.plugins.push(new CompressionWebpackPlugin())
            config.plugins.push(new HappyPack({
                id: 'happy-babel-js',
                loaders: ['babel-loader?cacheDirectory=true'],
                threadPool: happyThreadPool,
            }))
            config.plugins.push(new HappyPack({
                id: 'css-pack',
                loaders: ['css-loader']
            }))            
        }else{            
            config.plugins.push(new NodemonPlugin({
                watch: path.resolve('./mock'),                 
                script: './mock/serve.js',   
                ext: 'js'
            }))
        }
        config.externals = {
            // 'vue':'vue'
        }
    },
    css: {
        loaderOptions: {
          css: {
            data: `
              @import "@/assets/styles/index.scss";
            `
          },
          less: {
            javascriptEnabled: true
          }
        }
      }
}


