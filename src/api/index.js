
import admin from "./admin"
import v1 from "./v1"
import statis from "./statis"
import bos from "./bos"
import shopping from "./shopping"

const API = {
  ...admin,
  ...v1,
  ...statis,
  ...bos,
  ...shopping
}
export default API