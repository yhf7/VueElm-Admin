import { post, get } from '@/api/method'

const _defaultLoading = {
  _loading: true,
}

export default {
  // 登录
  getLogin(params) { return post('admin/login', params, _defaultLoading) },
  /**
  * 退出
  */
  Signout(params) { return get('admin/signout', params, _defaultLoading) },
  /**
  * 获取用户信息
  */
  getAdminInfo() { return get('admin/info') },
  /**
   * 管理员数量
   */
  getAdminCount() { return get('admin/count') },
  /**
   * 管理员列表
   */
  getAdminList(params) { return get('admin/all', params) },
}