import { get } from '@/api/method'

// const _defaultLoading = {
//   _loading: true,
// }

export default {
  /**
 * 用户注册量
 */
  getUserCount(params) { return get('statis/user/' + params + '/count') },
  /**
 * 某一天订单数量
 */
  getOrderCount(params) { return get('statis/order/' + params + '/count') },
  /**
 * 某一天管理员注册量
 */
  getAdminDayCount(params) { return get('statis/admin/' + params + "/count") },
  /**
 * api请求量
 */
  getApiCount(params) { return get('statis/api' + params + '/count') },
  /**
   * 所有api请求量
   */
  getApiAllCount() { return get('statis/api/count') },
  /**
 * 所有api请求信息
 */
  getApiAllRecord() { return get('statis/api/all') },
}
