import { get } from '@/api/method'

// const _defaultLoading = {
//   _loading: true,
// }

export default {
  /**
 * 获取订单数量
 */
  getOrderCounts() { return get('bos/orders/count') },
  /**
 * 获取订单列表
 */

getOrderList (params) {return get('/bos/orders', params)}
}
