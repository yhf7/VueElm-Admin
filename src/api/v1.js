import { get } from '@/api/method'

// const _defaultLoading = {
//   _loading: true,
// }

export default {
  /**
   * 获取用户数量
   */
  getUserCounts(params) { return get('v1/users/count', params) },
  /**
   * 获取用户列表
   */
  getUserList(params) { return get('v1/users/list', params) },
  /**
   * 获取定位城市
   */
  getCityGuess() { return get('v1/cities', { type: 'guess' }) },
  /**
   * 获取搜索地址
   */
  getSearchplace(cityid, value) { return get('v1/pois', { type: 'search', city_id: cityid, keyword: value }) },
  /**
   * 获取用户信息
   */
  getUserInfo(params) { return get('v1/user/' + params) },
  /**
   * 获取地址信息
   */
  getAddressById(params) { return get('v1/addresse/' + params) },

  /**
   * 获取用户分布信息
   */
  getUserCity() { return get('/v1/user/city/count') }
}
