import axios from 'axios'
import Qs from 'qs'
import Vue from 'vue'

// axios 配置
axios.defaults.timeout = 60000
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
if (process.env.NODE_ENV == 'development') {
    axios.defaults.baseURL = '/api'
}
const newvue = new Vue();
export function get(url, params, config) {
    return new Promise((resolve, reject) => {
        let $loading;
        if (config) {
            config._loading && ($loading = newvue.$$loading(config._text));
        }
        axios.get(url, { params: params })
            .then(response => {

                $loading && $loading.close();
                // if(response.data.indexOf&&response.data.indexOf('no privilege')>-1){
                //     $('body').append(response.data);
                //     return;
                // }
                if (response.data.code === 401) {
                    sessionStorage.clear();
                    location.href = response.data.data.url;
                    return false;
                }
                resolve(response.data)
            }, err => {
                $loading && $loading.close();
                reject(err)
            })
            .catch((error) => {
                $loading && $loading.close();
                reject(error)
            })
    })
}
/**
 * TODO  post请求
 * @param url 请求地址
 * @param params请求参数
 * @param config 请求是否要loading 
 * **/
export function post(url, params, config = {}) {
    console.log('data',params)
    return new Promise((resolve, reject) => {
        let $loading;
        if (config) {
            config._loading && ($loading = newvue.$$loading(config._text));
        }
        axios({
            url: url,
            method: 'post',
            data: params,
            timeout: config.timeout,
            headers: {
                'Accept': 'application/json',
                'Content-Type': config.json ? 'application/json' : 'application/x-www-form-urlencoded',
            },
            // transformRequest: [function (data) {
            //     if (config.json) return data;
            //     data = Qs.stringify(data);
            //     return data
            // }],
        }).then(response => {
            console.log('ress',response)
            // eslint-disable-next-line no-console
            // console.log(newvue)
            $loading && $loading.close();
            // if(response.data.indexOf&&response.data.indexOf('no privilege')>-1){
            //     $('body').append(response.data);
            //     return;
            // }

            if (response.data.code === 401) {
                sessionStorage.clear();
                location.href = response.data.data.url;
                return false;
            }
            resolve(response.data)
        }, err => {
            $loading && $loading.close();
            reject(err)
        })
            .catch((error) => {
                $loading && $loading.close();
                reject(error)
            })
    })
}

export function upload(url, params, config) {
    return new Promise((resolve, reject) => {
        let $loading;
        if (config) {
            config._loading && ($loading = newvue.$$loading(config._text));
        }
        axios({
            url: url,
            method: 'post',
            data: params,
            headers: {
                'Content-Type': 'multipart/form-data'
            },
        }).then(response => {
            $loading && $loading.close();
            if (response.data.code === 401) {
                sessionStorage.clear();
                location.href = response.data.data.url;
                return false;
            }
            resolve(response.data)
        }, err => {
            $loading && $loading.close();
            reject(err)
        })
            .catch((error) => {
                $loading && $loading.close();
                reject(error)
            })
    })
}

export function del(url, config) {
    return new Promise((resolve, reject) => {
        let $loading;
        if (config) {
            config._loading && ($loading = newvue.$$loading(config._text));
        }
        axios.delete(url)
            .then(response => {

                $loading && $loading.close();
                // if(response.data.indexOf&&response.data.indexOf('no privilege')>-1){
                //     $('body').append(response.data);
                //     return;
                // }
                if (response.data.code === 401) {
                    sessionStorage.clear();
                    location.href = response.data.data.url;
                    return false;
                }
                resolve(response.data)
            }, err => {
                $loading && $loading.close();
                reject(err)
            })
            .catch((error) => {
                $loading && $loading.close();
                reject(error)
            })
    })
}
