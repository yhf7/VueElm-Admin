import { get, post, del } from '@/api/method'

// const _defaultLoading = {
//   _loading: true,
// }

export default {
  /**
 * 添加商铺
 */
  getAddShop(params) { return post('shopping/addShop', params) },
  /**
   * 获取当前店铺食品种类
   */
  getCategory(params) { return get('shopping/getcategory/' + params) },
  /**
 * 添加食品种类
 */

  addCategory(params) { return post('shopping/addcategory', params) },


  /**
   * 添加食品
   */

  addFood(params) { return post('shopping/addfood', params) },


  /**
   * category 种类列表
   */

  getFoodCategory() { return get('shopping/v2/restaurant/category') },

  /**
   * 获取餐馆列表
   */

  getResturants(params) { return get('shopping/restaurants', params) },

  /**
   * 获取餐馆详细信息
   */

  getResturantDetail(params) { return get('shopping/restaurant/' + params) },

  /**
   * 获取餐馆数量
   */

  getResturantsCount() { return get('shopping/restaurants/count') },

  /**
   * 更新餐馆信息
   */

  updateResturant(params) { return post('shopping/updateshop', params) },

  /**
   * 删除餐馆
   */

  deleteResturant(params) { return del('shopping/restaurant/' + params, {}) },

  /**
   * 获取食品列表
   */

  getFoods(params) { return get('shopping/v2/foods', params) },

  /**
   * 获取食品数量
   */

  getFoodsCount(params) { return get('shopping/v2/foods/count', params) },


  /**
   * 获取menu列表
   */

  getMenu(params) { return get('shopping/v2/menu', params) },

  /**
   * 获取menu详情
   */

  getMenuById(params) { return get('shopping/v2/menu/' + params) },

  /**
   * 更新食品信息
   */

  setUpdateFood(params) { return post('shopping/v2/updatefood', params) },

  /**
   * 删除食品
   */

  deleteFood(params) { return del('shopping/v2/food/' + params, {}) }
}
