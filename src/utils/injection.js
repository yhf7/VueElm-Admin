import Vue from 'vue'
import { Loading } from 'element-ui';

//注册弹窗方法
/**
 * TODO 公用弹窗方法
 * @options Object 
 * **/
let _defaultLoading = {
  background: 'rgba(0, 0, 0, 0.6)',
  text: '玩命加载中...',
  spinner: 'el-icon-loading',
}

Vue.prototype.$$loading = function(options) {
  options = options ? (typeof options === 'string' ? { text: options } : options) : {};
  options = Object.assign({},_defaultLoading, options)
  return Loading.service(options);
}
