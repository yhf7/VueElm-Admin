import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 按需引入
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-default/index.css'
import * as OfflinePluginRuntime from 'offline-plugin/runtime';
if(process.env.NODE_ENV === 'production'){
    OfflinePluginRuntime.install();
}
Vue.config.productionTip = false

// element 配置
Vue.use(ElementUI);

// 工具配置
import '@/utils/injection.js'
// 初始化
import '@/assets/styles/normalize_8.0.0.css'
import '@/assets/styles/index.less'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
