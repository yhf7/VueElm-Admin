const state = {
    items: []
}
// mutations
const mutations = {
    addItems(state, { id }) {
        state.items.push({
            id,
            quantity: 1
        })
    }
}

// actions
const actions = {
    addItems({ commit }, row) {
        commit('addItems', row)
    }
}

// getters
const getters = {
    items: (state) => {
        return state.items
    }
}
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
