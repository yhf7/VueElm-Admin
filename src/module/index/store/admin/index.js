const state = {
  loginkey: 'a'
}

const SET_LOGIN_KEY = 'SET_LOGIN_KEY'
// mutations
const mutations = {
  [SET_LOGIN_KEY](state, status) {
    state.loginkey = status
  }
}

// actions
const actions = {
  setLoginKey({ commit }, status) {
    commit('SET_LOGIN_KEY', status)
  }
}

// getters
const getters = {
  getLoginKey: (state) => {
    return state.loginkey
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
