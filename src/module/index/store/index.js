import Vue from 'vue'
import Vuex from 'vuex'
import API from '@/api/index'
Vue.use(Vuex)

const state = {
    loginkey: '',
    adminInfo: {
        avatar: 'default.jpg'
    }
}

const SET_LOGIN_KEY = 'SET_LOGIN_KEY'
// mutations
const mutations = {
    [SET_LOGIN_KEY](state, status) {
        state.loginkey = status
    },
    saveAdminInfo(state, adminInfo) {
        state.adminInfo = adminInfo;
    },
    updateAppSetting(state,status) {
        state.allowBack = status.allowBack
    }
}

// actions
const actions = {
    setLoginKey({ commit }, status) {
        commit('SET_LOGIN_KEY', status)
    },
    async getAdminData({ commit }) {
        try {
            const res = await API.getAdminInfo()
            if (res.status == 1) {
                commit('saveAdminInfo', res.data);
            } else {
                throw new Error(res.type)
            }
        } catch (err) {
            // console.log(err.message)
        }
    },
    updateAppSetting({commit}, status){
        commit('updateAppSetting', status)
    }
}

// getters
const getters = {
    getLoginKey: (state) => () => {
        return state.loginkey
    }

}

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})
