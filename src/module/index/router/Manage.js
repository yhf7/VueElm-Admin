const Manage = () => import( /* webpackChunkName: "pages-Manage" */ '@/pages/Manage')//主页侧边栏
const Home = () => import( /* webpackChunkName: "pages-manage-Home" */ '@/pages/Manage/Home')//主页内容
const UserList = () => import( /* webpackChunkName: "pages-manage-UserList" */ '@/pages/Manage/UserList')//用户列表
const ShopList = () => import( /* webpackChunkName: "pages-manage-ShopList" */ '@/pages/Manage/ShopList')//商家列表
const FoodList = () => import( /* webpackChunkName: "pages-manage-FoodList" */ '@/pages/Manage/FoodList')//食品列表
const OrderList = () => import( /* webpackChunkName: "pages-manage-OrderList" */ '@/pages/Manage/OrderList')//订单列表
const AdminList = () => import( /* webpackChunkName: "pages-manage-AdminList" */ '@/pages/Manage/AdminList')//管理员列表
const AddShop = () => import( /* webpackChunkName: "pages-manage-AddShop" */ '@/pages/Manage/AddShop')//订单列表
const AddGoods = () => import( /* webpackChunkName: "pages-manage-AddGoods" */ '@/pages/Manage/AddGoods')//管理员列表

const Visitor = () => import( /* webpackChunkName: "pages-manage-Visitor" */ '@/pages/Manage/Visitor')//用户分布
const VueEdit = () => import( /* webpackChunkName: "pages-manage-VueEdit" */ '@/pages/Manage/VueEdit')//文本编辑
const AdminSet = () => import( /* webpackChunkName: "pages-manage-AdminSet" */ '@/pages/Manage/AdminSet')//管理员设置
const Explain = () => import( /* webpackChunkName: "pages-manage-Explain" */ '@/pages/Manage/Explain')//说明

export default [
  {
    // 后台内部页面
    path: '/Manage',
    name: 'Manage',
    component: Manage,
    meta: { allowBack: false },
    children: [
      {
        path: "",
        component: Home,
        name: Home,
        meta: {
          user: []
        }
      },
      // 管理
      {
        path: "/UserList",
        component: UserList,
        name: UserList,
        meta: {
          user: ["管理数据", "用户列表"]
        }
      },
      {
        path: "/ShopList",
        component: ShopList,
        name: ShopList,
        meta: {
          user: ["管理数据", "商家列表"]
        }
      },
      {
        path: "/FoodList",
        component: FoodList,
        name: FoodList,
        meta: {
          user: ["管理数据", "食品列表"]
        }
      },
      {
        path: "/OrderList",
        component: OrderList,
        name: OrderList,
        meta: {
          user: ["管理数据", "订单列表"]
        }
      },
      {
        path: "/AdminList",
        component: AdminList,
        name: AdminList,
        meta: {
          user: ["管理数据", "管理员列表"]
        }
      },
      {
        path: "/AddShop",
        component: AddShop,
        name: AddShop,
        meta: {
          user: ["添加数据", "添加商铺"]
        }
      },
      {
        path: "/AddGoods",
        component: AddGoods,
        name: AddGoods,
        meta: {
          user: ["添加数据", "添加商品"]
        }
      },
      {
        path: '/Visitor',
        component: Visitor,
        name: Visitor,
        meta: {user: ['图表', '用户分布']},
      },
      {
        path: '/VueEdit',
        component: VueEdit,
        name: VueEdit,
        meta: {user: ['编辑', '文本编辑']},
      },
      {
        path: '/AdminSet',
        component: AdminSet,
        name: AdminSet,
        meta: {user: ['设置', '管理员设置']},
      },
      {
        path: '/Explain',
        component: Explain,
        name: Explain,
        meta: {user: ['说明', '说明']},
      }
    ]
  }
]
