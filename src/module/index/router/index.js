import Vue from 'vue'
import Router from 'vue-router'
import Login from './Login' // 登录
import Manage from './Manage' // 系统内容
import vuex from "../store";
import store from "../store"
Vue.use(Router);

let router =  new Router({
    mode: 'history',// 真路由，去除多余的#号
    routes: [
        ...Login,
        ...Manage
    ]
})


// 全局路由守护
// router.beforeEach((to,from,next) => {
//     let allowBack = true    //    给个默认值true
//     if (to.meta.allowBack !== undefined) {
//         allowBack = to.meta.allowBack
//     }
//     if (!allowBack) {
//         history.pushState(null, null, location.href)
//     }
//     store.dispatch('updateAppSetting', {     //   updateAppSetting 只是store里面的一个action， 用来改变store里的allowBack的值的，具体怎么改这个值 要根据各位的实际情况而定
//         allowBack: allowBack
//     })
//     if (to.name != 'Login' && (vuex.getters.getLoginKey()) != 'yhfkey') {
//         next({name: 'Login'})
//     }
//     next()
// })

export default router