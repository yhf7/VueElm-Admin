const Login = ()=> import( /* webpackChunkName: "login-page" */ '@/pages/Login')//登录

export default [
    {
        path: '/',
        name: 'Login',
        component: Login,
        meta: { allowBack: false }
    }
]
