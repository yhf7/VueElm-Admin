import { Loading } from 'element-ui';
import Vue from 'vue'
import _ from 'lodash';

//loading加载效果
let _defaultLoading = {
    background: 'rgba(0, 0, 0, 0.7)',
    text: '加载中...',
    spinner: 'el-icon-loading',
}

Vue.prototype.$$loading = function (options) {
    options = options || _defaultLoading;
    options = Object.assign(_defaultLoading, typeof options === 'string' ? { text: options } : options)
    return Loading.service(options);
} 

Object.assign(window, {
    log: process.env.NODE_ENV === 'development' ? console.log.bind(console) : () => { },
})
Object.assign(window, {
    _: _
})